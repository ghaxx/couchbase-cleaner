# Usage

1. Provide CouchbaseCleaner with a proper function

```
    {
      case BucketName("bucket-name") => 
        documentKey: String => predicate(documentKey)
    }
```

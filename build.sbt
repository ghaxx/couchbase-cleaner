import sbt._
import sbt.Keys._

scalaVersion := "2.11.8"

organization := "grandparade"
name := "couchbase-cleaner"
version := "1.0"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-feature", "-language:postfixOps")

val akkaV = "2.4.9"
libraryDependencies ++= Seq(
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "com.typesafe.akka" %% "akka-actor" % akkaV,
  "com.typesafe.akka" %% "akka-slf4j" % akkaV,
  "com.typesafe.akka" %% "akka-stream" % akkaV,
  "com.couchbase.client" % "java-client" % "2.2.6",
  "org.json4s" %% "json4s-native" % "3.3.0",
  "io.reactivex" % "rxjava" % "1.1.8",
  "io.reactivex" % "rxjava-reactive-streams" % "1.0.0",
  "org.scalatest" %% "scalatest" % "2.2.4",
  "com.twitter" %% "bijection-core" % "0.8.1",
  "com.typesafe.akka" %% "akka-testkit" % akkaV
)

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

credentials += Credentials("Artifactory Realm", "gpp-artifactory.gp-cloud.com", "jenkins", "WpDM9+uTLkXN$*p?tNS=5Le^")

resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += "Couchbase Maven Repository" at "http://files.couchbase.com/maven2"
resolvers += "clojars.org" at "http://clojars.org/repo"
resolvers += Resolver.url("Artifactory Realm Realeases",
  url("https://gpp-artifactory.gp-cloud.com/artifactory/gpp-libs-releases"))(Resolver.ivyStylePatterns)
resolvers += Resolver.url("Artifactory Realm Snapshots",
  url("https://gpp-artifactory.gp-cloud.com/artifactory/gpp-libs-snapshots"))(Resolver.ivyStylePatterns)

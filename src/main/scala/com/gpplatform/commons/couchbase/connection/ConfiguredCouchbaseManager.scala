package com.gpplatform.commons.couchbase.connection

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import com.couchbase.client.java.env.DefaultCouchbaseEnvironment
import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConverters._
import scala.util.Try

class ConfiguredCouchbaseManager(config: Config)(implicit materializer: Materializer) extends CouchbaseManager {

  private val cluster = new ManagedCluster(ConfiguredCouchbaseManager.environment, config.getString("host"))
  private val buckets: Map[String, ManagedBucket] = config.getConfigList("buckets").asScala.map(this.createBucket).toMap
  private def createBucket(cfg: Config) = {
    val id = cfg.getString("id")
    val name = cfg.getString("bucket")
    val password = cfg.getString("password")
    val bucket = new ManagedBucket(ConfiguredCouchbaseManager.environment, cluster, name, password)
    (id, bucket)
  }

  override def getBucket(id: String): ManagedBucket = buckets(id)
  override def getBuckets(): List[ManagedBucket] = buckets.values.toList
  override def disconnect() = cluster.disconnect()
}

object ConfiguredCouchbaseManager {

  private lazy val config = ConfigFactory.load()

  private lazy val computationPoolSize = Try(config.getInt("couchbase.computationPoolSize")) getOrElse 3
  private lazy val ioPoolSize = Try(config.getInt("couchbase.ioPoolSize")) getOrElse 3

  private lazy val environment = DefaultCouchbaseEnvironment.builder()
    .computationPoolSize(computationPoolSize)
    .ioPoolSize(ioPoolSize)
    .bufferPoolingEnabled(false)
    .build()

  def forActorSystem(system: ActorSystem) = {
    implicit val materializer = ActorMaterializer.create(system)
    val manager = new ConfiguredCouchbaseManager(system.settings.config.getConfig("couchbase"))
    system.registerOnTermination {
      manager.disconnect()
    }
    manager
  }
}

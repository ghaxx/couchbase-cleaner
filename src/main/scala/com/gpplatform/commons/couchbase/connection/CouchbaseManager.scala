package com.gpplatform.commons.couchbase.connection

trait CouchbaseManager {
  def getBucket(id: String): ManagedBucket
  def getBuckets(): List[ManagedBucket]
  def disconnect()
}

package com.gpplatform.commons.couchbase.connection

import java.util
import java.util.{Timer, TimerTask}

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import com.couchbase.client.java.document.{Document, JsonDocument, JsonLongDocument, RawJsonDocument}
import com.couchbase.client.java.env.CouchbaseEnvironment
import com.couchbase.client.java.error.{DocumentAlreadyExistsException, DocumentDoesNotExistException, DurabilityException, TemporaryFailureException}
import com.couchbase.client.java.view.{AsyncViewRow, ViewQuery}
import com.couchbase.client.java.{AsyncBucket, Bucket, CouchbaseBucket, ReplicateTo}
import com.typesafe.scalalogging.LazyLogging
import rx.functions.Func1
import rx.{Observable, RxReactiveStreams}

import scala.collection.JavaConverters._
import scala.concurrent._
import scala.concurrent.duration._


/**
  * Lazy-opened bucket that manages low-level details like (not implemented yet!):
  * - retries and re-connections to the cluster
  * - provides convenience methods for bucket operations that return Scala types (Futures instead of RxJava Observables)
  */
class ManagedBucket(val environment: CouchbaseEnvironment, val cluster: ManagedCluster,
                         val name: String, val password: String, val ensureReplicatedByDefault: Boolean = false)
                        (implicit val flowMaterializer: Materializer) extends LazyLogging {

  @volatile private var instance: Future[AsyncBucket] = null
  private var syncInstance: Bucket = null

  private val delayAmount = (retry: Int) => Math.pow(2, retry - 1).toLong * 250

  lazy val t = new Timer(true)

  def get(id: String)(implicit executor: ExecutionContext): Future[RawJsonDocument] =
    get(id, classOf[RawJsonDocument])

  def get[T <: Document[_]](id: String, target: Class[T])(implicit executor: ExecutionContext): Future[T] = {
    async.flatMap(asyncBucket => {
      val source: Source[T, NotUsed] = Source.fromPublisher(RxReactiveStreams.toPublisher(asyncBucket.get(id, target)))
      val document: Future[T] = source.runWith(Sink.head)
      document
    })
  }


  def getByIds(ids: Seq[String])(implicit executor: ExecutionContext): Future[Seq[RawJsonDocument]] =
    getByIds(ids, classOf[RawJsonDocument])

  def getByIds[T <: Document[_]](ids: Seq[String], target: Class[T])(implicit executor: ExecutionContext): Future[Seq[T]] = {
    async.flatMap(asyncBucket => {

      val observable = Observable.from(ids.toArray).flatMap(new Func1[String, Observable[T]] {
        override def call(id: String): Observable[T] = asyncBucket.get(id, target)
      }).toList

      val source: Source[util.List[T], NotUsed] = Source.fromPublisher(RxReactiveStreams.toPublisher(observable))
      val documents: Future[util.List[T]] = source.runWith(Sink.head)
      documents.map(_.asScala.toSeq)
    })
  }


  def insert[T <: Document[_]](document: T)(implicit executor: ExecutionContext): Future[T] =
    insert(document, ensureReplicatedByDefault)

  def insert[T <: Document[_]](document: T, ensureReplicated: Boolean)(implicit executor: ExecutionContext): Future[T] = {
    recoverWithRetry(async.flatMap(asyncBucket => {
      val insertedDocumentObservable = asyncBucket.insert(document, calcReplicateTo(ensureReplicated))
      val insertedDocumentFuture = Source.fromPublisher(RxReactiveStreams.toPublisher(insertedDocumentObservable)).runWith(Sink.head)
      insertedDocumentFuture
    }),
      s"""Couchbase insert error""", document.id())
  }

  def replace[T <: Document[_]](document: T)(implicit executor: ExecutionContext): Future[T] =
    replace(document, ensureReplicatedByDefault)

  def replace[T <: Document[_]](document: T, ensureReplicated: Boolean)(implicit executor: ExecutionContext): Future[T] = {
    recoverWithRetry(async.flatMap(asyncBucket => {
      val insertedDocumentObservable = asyncBucket.replace(document, calcReplicateTo(ensureReplicated))
      val insertedDocumentFuture = Source.fromPublisher(RxReactiveStreams.toPublisher(insertedDocumentObservable)).runWith(Sink.head)
      insertedDocumentFuture
    }),
      s"""Couchbase replace error""", document.id())
  }

  def upsert[T <: Document[_]](document: T)(implicit executor: ExecutionContext): Future[T] =
    upsert(document, ensureReplicatedByDefault)

  def upsert[T <: Document[_]](document: T, ensureReplicated: Boolean)(implicit executor: ExecutionContext): Future[T] = {
    recoverWithRetry(async.flatMap(asyncBucket => {
      val insertedDocumentObservable = asyncBucket.upsert(document, calcReplicateTo(ensureReplicated))
      val insertedDocumentFuture = Source.fromPublisher(RxReactiveStreams.toPublisher(insertedDocumentObservable)).runWith(Sink.head)
      insertedDocumentFuture
    }),
      s"""Couchbase upsert error""", document.id())
  }

  def counter(id: String, delta: Long, initial: Long)(implicit executor: ExecutionContext): Future[JsonLongDocument] =
    counter(id, delta, initial, ensureReplicatedByDefault)

  def counter(id: String, delta: Long, initial: Long, ensureReplicated: Boolean)(implicit executor: ExecutionContext): Future[JsonLongDocument] = {
    recoverWithRetry(async.flatMap(asyncBucket => {
      val updatedCounterObservable = asyncBucket.counter(id, delta, initial, calcReplicateTo(ensureReplicated))
      val updatedCounterFuture = Source.fromPublisher(RxReactiveStreams.toPublisher(updatedCounterObservable)).runWith(Sink.head)
      updatedCounterFuture
    }),
      s"""Couchbase counter error""", id)
  }

  def remove[T <: Document[_]](id: String, target: Class[T])(implicit executor: ExecutionContext): Future[T] =
    remove(id, target, ensureReplicatedByDefault)

  def remove[T <: Document[_]](id: String, target: Class[T], ensureReplicated: Boolean)(implicit executor: ExecutionContext): Future[T] = {
    recoverWithRetry(async.flatMap(asyncBucket => {
      val source: Source[T, NotUsed] = Source.fromPublisher(RxReactiveStreams.toPublisher(asyncBucket.remove(id, calcReplicateTo(ensureReplicated), target)))
      val future: Future[T] = source.runWith(Sink.head)
      future
    }),
      s"""Couchbase remove error""", id)
  }

  def remove(id: String)(implicit executor: ExecutionContext): Future[JsonDocument] =
    remove(id, ensureReplicatedByDefault)

  def remove(id: String, ensureReplicated: Boolean)(implicit executor: ExecutionContext): Future[JsonDocument] =
    remove(id, classOf[JsonDocument], ensureReplicated)

  def query(q: ViewQuery)(implicit executor: ExecutionContext): Future[List[AsyncViewRow]] = {
    async.flatMap(b => {
      val asyncViewResult = b.query(q)
      Source.fromPublisher(RxReactiveStreams.toPublisher(asyncViewResult)).runWith(Sink.head)
    }).flatMap(asyncViewResult => {
      Source.fromPublisher(RxReactiveStreams.toPublisher(asyncViewResult.rows())).runWith(Sink.fold(List[AsyncViewRow]())((l, e) => e :: l)).map(_.reverse)
    })
  }

  // at the moment it is not possible to get a sync bucket from an async
  def async = {
    if (instance == null || (instance.isCompleted && instance.value.isEmpty)) {
      val producer = RxReactiveStreams.toPublisher(cluster.get.openBucket(name, password))
      instance = Source.fromPublisher(producer).runWith(Sink.head)
      logger.info("AcyncBucket is created")
    }
    instance
  }

  /**
    * Returns synchronous Bucket. Prefer the asynchronous interface because it is non-blocking.
    */
  def sync = {
    if (syncInstance == null) {
      val asyncCluster = cluster.get
      val trans = new util.ArrayList()
      Await.result(async, 5.minutes)
      syncInstance = new CouchbaseBucket(environment, asyncCluster.core().toBlocking.single(), name, password,
        trans.asInstanceOf[java.util.List[com.couchbase.client.java.transcoder.Transcoder[_ <: com.couchbase.client.java.document.Document[_], _]]])
    }
    syncInstance
  }

  private val calcReplicateTo = (ensured: Boolean) ⇒ {
    if (ensured && this.cluster.nodes.length > 1) {
      ReplicateTo.ONE
    } else {
      ReplicateTo.NONE
    }
  }

  def recoverWithRetry[T](f: => Future[T], prefix: String = "", docId: String = "")(implicit ec: ExecutionContext): Future[T] =
    retry(f, 1, 4, prefix, docId)

  def retry[T](f: => Future[T], attempt: Int, attempts: Int, prefix: String, docId: String)(implicit ec: ExecutionContext): Future[T] = {
    f recoverWith {
      case _: TemporaryFailureException if attempt < attempts ⇒ delay(delayAmount(attempt))(retry(f, attempt + 1, attempts, prefix, docId))
      case _: DocumentAlreadyExistsException ⇒ Future.failed(throw new DocumentAlreadyExistsException(s"""$prefix: Document "$docId" already exist in bucket "${this.name}"."""))
      case _: DocumentDoesNotExistException ⇒ Future.failed(throw new DocumentDoesNotExistException(s"""$prefix: Document "$docId" does not exist in bucket "${this.name}"."""))
      case _: TemporaryFailureException ⇒ Future.failed(throw new TemporaryFailureException(s"""$prefix: Document "$docId" can't be saved "${this.name} due to temporary issues"."""))
      case e: DurabilityException ⇒
        val cause = if (e.getCause == null) {
          ""
        } else {
          s"Caused by: ${e.getCause.toString}."
        }
        Future.failed(throw new DurabilityException(s"""$prefix: Document "$docId" could not be durably updated in bucket "${this.name}". Tried $attempts times. ${e.getMessage}. $cause"""))
    }
  }

  private def delay[T](delay: Long)(block: => Future[T]): Future[T] = {
    val promise = Promise[T]()

    t.schedule(new TimerTask {
      override def run(): Unit = {
        promise.completeWith(block)
      }
    }, delay)
    promise.future
  }

}

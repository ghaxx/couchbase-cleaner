package com.gpplatform.commons.couchbase.connection

import com.couchbase.client.java.CouchbaseAsyncCluster
import com.couchbase.client.java.env.CouchbaseEnvironment


class ManagedCluster(environment: CouchbaseEnvironment, hosts: String) {

  private var instance: CouchbaseAsyncCluster = null

  val nodes = hosts.split(",").map(_.trim)

  def get = {
    if (instance == null) {
      instance = CouchbaseAsyncCluster.create(environment, nodes: _*)
    }
    instance
  }

  def disconnect() = {
    if (instance != null) {
      instance.disconnect()
    }
  }

}

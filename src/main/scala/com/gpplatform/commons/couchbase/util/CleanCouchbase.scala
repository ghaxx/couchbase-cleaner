package com.gpplatform.commons.couchbase.util

import java.util.concurrent.atomic.AtomicInteger

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.couchbase.client.java.view._
import com.gpplatform.commons.couchbase.connection.{ConfiguredCouchbaseManager, CouchbaseManager, ManagedBucket}
import com.gpplatform.commons.couchbase.util.CleanCouchbase.BucketName
import com.gpplatform.commons.couchbase.util.stats.{BucketCleaningStats, CouchbaseCleaningStats}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

object CleanCouchbase extends LazyLogging {

  private val config = ConfigFactory.load()
  private implicit val system = ActorSystem("Cleaner")
  private implicit val materializer = ActorMaterializer()

  private def couchbase: CouchbaseManager = new ConfiguredCouchbaseManager(config.getConfig("couchbase"))

  private implicit def executionContext: ExecutionContext = system.dispatcher

  private val defaultViewCreator = new DefaultViewCreator(couchbase)

  /**
    * @param cleanFilters This function applied on bucket should return another function. This one will be applied on
    *                     a document key and return `true` if the document should be deleted.
    */
  def apply(cleanFilters: PartialFunction[BucketName, Function[DocumentKey, Boolean]]): Unit = {
    logger.info("Creating default views")
    defaultViewCreator.createDefaultViews()
    logger.info("Going to clean Couchbase")
    clearBuckets(cleanFilters.orElse {
      case _ => (_: DocumentKey) => false
    }).map { stats =>
      stats.lines.foreach(logger.info(_))
      system.terminate()
    }
    Await.result(system.whenTerminated, 60 seconds)
  }

  private def clearBuckets(filters: PartialFunction[BucketName, DocumentKey => Boolean]): Future[CouchbaseCleaningStats] = {
    couchbase.getBuckets().foldLeft(Future.successful(new CouchbaseCleaningStats())) { (f, bucket) =>
      logger.debug(s"Cleaning bucket ${bucket.name}")
      for {
        stats <- f
        bucketStats <- clearBucket(bucket, filters(BucketName(bucket.name)))
      } yield stats.append(bucketStats)
    }
  }

  private def clearBucket(bucket: ManagedBucket, filter: DocumentKey => Boolean): Future[BucketCleaningStats] = {
    val vq = ViewQuery.from("documents", "all").stale(Stale.FALSE).debug()
    val rowIDs = bucket.query(vq).map { rows =>
      rows.map { row =>
        logger.debug(s"Found: ${row.id()}")
        row.id()
      }
    }
    Await.result(rowIDs, 1 minute).foldLeft(Future.successful(new BucketCleaningStats(BucketName(bucket.name)))) { (f, rowId) =>
      try {
        if (filter(DocumentKey(rowId))) {
          logger.debug(s"Removing: $rowId")
          for {
            stats <- f
            _ <- bucket.remove(rowId)
          } yield stats.inc()
        } else {
          f
        }
      } catch {
        case e: Throwable =>
          logger.error(s"Error while removing $rowId from ${bucket.name}: ${e.getMessage}")
          Future.failed(e)
      }
    }
  }

  case class BucketName(value: String) {
    override def toString: String = value
  }

  case class DocumentKey(value: String) {
    override def toString: String = value
  }





}
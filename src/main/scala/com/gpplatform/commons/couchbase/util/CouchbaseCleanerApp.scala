package com.gpplatform.commons.couchbase.util

import com.gpplatform.commons.couchbase.util.CleanCouchbase.BucketName

object CouchbaseCleanerApp extends App {

  CleanCouchbase {
    case BucketName("trading-test") =>
      key => List("C:", "Lookup:", "CPID:", "E:", "EPID:", "M:", "IDSeq:E", "IDSeq:C", "P:", "PPID:").exists(key.value.startsWith)
    //      key => true
    case BucketName("centralizer-test") =>
      key => List("E:", "C:", "P:", "CID:", "Lookup:", "PID:", "ID:C", "ID:E", "ID:P", "M:", "EID:").exists(key.value.startsWith)
    //      key => true
  }

}

package com.gpplatform.commons.couchbase.util

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.couchbase.client.java.view.{DefaultView, DesignDocument, View}
import com.gpplatform.commons.couchbase.connection.{ConfiguredCouchbaseManager, CouchbaseManager}
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext

class DefaultViewCreator(couchbase: CouchbaseManager)(implicit executionContext: ExecutionContext) extends LazyLogging {

  import scala.collection.JavaConversions._

  type BucketName = String
  type DesignDocumentName = String
  type ViewName = String

  def createDefaultViews() =
    defaultCouchbaseViews().foreach(b => createDesignDocuments(b._1, b._2))

  def defaultCouchbaseViews(): Map[BucketName, Map[DesignDocumentName, List[ViewName]]] = {
    couchbase.getBuckets().foldLeft(
      Map[BucketName, Map[DesignDocumentName, List[ViewName]]]())(
      (m, b) => m ++ Map(b.name.replace("-test", "") -> Map("documents" -> List("all"))))
  }

  def createDesignDocuments(bucketName: String, designDocuments: Map[DesignDocumentName, List[ViewName]]) = {
    logger.debug(s"Create design document ${designDocuments.keys} for bucket $bucketName")
    val bucket = couchbase.getBucket(bucketName).sync.bucketManager()
    designDocuments
      .map(docWithViewNames => constructDesignDocument(docWithViewNames._1, docWithViewNames._2))
      .map(bucket.upsertDesignDocument)
  }

  private def constructDesignDocument(name: String, viewNames: List[String]) = {
    val views = viewNames.map(viewName => constructViewDesign(viewName, name))
    DesignDocument.create(name, views)
  }

  private def constructViewDesign(name: String, designDocumentName: String): View = {
    val mapFile = s"/couchbase/views/$designDocumentName/$name/map.js"
    val reduceFile = s"/couchbase/views/$designDocumentName/$name/reduce.js"

    logger.debug(s"Before creation a view design - name: $name, design document: $designDocumentName")

    val map = io.Source.fromInputStream(getClass.getResourceAsStream(mapFile)).mkString
    val reduce = io.Source.fromInputStream(getClass.getResourceAsStream(reduceFile)).mkString

    logger.debug(s"View design created - name: $name, design document: $designDocumentName")

    DefaultView.create(name, map, reduce)
  }
}

package com.gpplatform.commons.couchbase.util.stats

import java.util.concurrent.atomic.AtomicInteger

import com.gpplatform.commons.couchbase.util.CleanCouchbase.BucketName

class BucketCleaningStats(bucket: BucketName) {
  val removedDocumentsCount = new AtomicInteger(0)

  def inc() = {
    removedDocumentsCount.incrementAndGet()
    this
  }

  def lines: List[String] = List(s"$bucket: removed ${removedDocumentsCount.get()}")

  override def toString: String = {
    s"$bucket: removed ${removedDocumentsCount.get()}"
  }
}

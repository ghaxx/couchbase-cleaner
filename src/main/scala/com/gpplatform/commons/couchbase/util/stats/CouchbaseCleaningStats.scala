package com.gpplatform.commons.couchbase.util.stats;

class CouchbaseCleaningStats() {
  val stats = scala.collection.mutable.ListBuffer.empty[BucketCleaningStats]

  def append(bucketStats: BucketCleaningStats) = {
    stats += bucketStats
    this
  }

  def lines: List[String] = {
    "Cleaning stats:" :: stats.toList.flatMap(_.lines).map(" - " + _)
  }

  override def toString: String = {
    lines.mkString("\n")
  }
}